# License ![Creative Commons CC-BY-SA-4.0 License Logo](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

The license applies to Tim Weber’s “org” repository containing Tim’s public todos and diary.

At the time of writing (2020-09-21), that repository’s canonical location on the web is <https://codeberg.org/scy/org>.
