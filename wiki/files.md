# How I manage my files

## Categorization based on size and frequency of use

### Briefcase

Things that I use regularly and that I should basically have always with me.
This means that they should be on at least one of the devices I carry with me.

A consequence of this is that the files should not be too large.
The briefcase is supposed to be smaller than 5 to 10 GB.

It should include:

* Notes, to-do lists, personal wiki.
* Documents (business and private) of the last 2 years: Invoices, letters, receipts, etc.
* Recent or important pictures: Basically everything that’s not yet in the pictures archive.
* My usual avatar images for when I sign up somewhere.
* Manuals for things I own that might come in handy.
* Memes and reaction GIFs.
* I’ve always wanted to have a collection of clips for a soundboard-like thingie …

### Attic

These are files that I want to (or even have to) keep for years or until the rest of my life, but which are too large or too many to keep on internal storage.

For example, the archive of photos (and increasingly, videos) taken by myself is already over 500 GB in size and will only continue to grow, as I am never throwing any of them away.
They are important to me and I want to have them within arm’s reach, but I don’t want to carry them around when I’m traveling “light”.

The attic should fit on a single USB-powered external drive and thus be no larger than about 2 TB in size.

So, things that fit into this category are:

* Photos and video by myself as well as friends and family.
* Music.
* Software installers, ISO files, OS images.
* “Takeouts” (i.e. data exports from sites and services).
* Talks I held, presentations or interviews I gave, be it in PDF, audio or video form.
* Old projects.
* Other content that I like having around or have to tidy up and sort into other places (as long as it’s not too large).

### Vault

That’s the place for large blobs of data that I might need at some point, but mostly keep around for archival or safety purposes.

It’s the category where ≥ 10 TB disks are used.

* Large video content like archives of YouTube playlists, my own stream recordings or raw files.
* Backups, disk images etc.
